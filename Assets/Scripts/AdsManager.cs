﻿using UnityEngine;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour {

    static InterstitialAd interstitial, startInterstitial;
    public static bool showStartInterstitial = true, showInterstitial = false, first = true, previusState;
    public GameObject downloadScreen, startScreen, deadScreen;
    public static float time = 0;

    void Start() {
        print(first);
        if (first && PlayerPrefs.GetInt("BuyNoAds") != 1) {
            RequestStartInterstitial();
            RequestInterstitial();
            first = false;
        }
        if (PlayerPrefs.GetInt("BuyNoAds") == 1) {
            if (first) {
                first = false;
                showStartInterstitial = false;
                startScreen.GetComponent<StartScreenController>().View();
            }
        }
    }

    void Update() {
        if (PlayerPrefs.GetInt("BuyNoAds") != 1) {
            if (downloadScreen.activeSelf)
            {
                time += Time.deltaTime;
            }
            else
            {
                time = 0;
            }
            if (interstitial.IsLoaded() && showInterstitial)
            {
                interstitial.Show();
                downloadScreen.SetActive(false);
                RequestInterstitial();
                showInterstitial = false;
                deadScreen.GetComponent<DeadScreenController>().View();
            }
            else if (showInterstitial)
            {
                downloadScreen.SetActive(true);
            }
            if (startInterstitial.IsLoaded() && showStartInterstitial)
            {
                startInterstitial.Show();
                downloadScreen.SetActive(false);
                showStartInterstitial = false;
                startScreen.GetComponent<StartScreenController>().View();
            }
            else if (showStartInterstitial)
            {
                downloadScreen.SetActive(true);
            }
            if (time > 5f)
            {
                downloadScreen.SetActive(false);
                if (showInterstitial)
                {
                    deadScreen.GetComponent<DeadScreenController>().View();
                    showInterstitial = false;
                }
                else
                {
                    startScreen.GetComponent<StartScreenController>().View();
                    showStartInterstitial = false;
                }
            }
        } 
    }

    void RequestStartInterstitial() {
        string adUnitId = "ca-app-pub-4030265201649343/1109549319";
        startInterstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        startInterstitial.LoadAd(request);
    }

    void RequestInterstitial() {
        string adUnitId = "ca-app-pub-4030265201649343/2505557313";
        interstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }
}
