﻿using UnityEngine;

public class ColorSwitchGlassSplinter : MonoBehaviour {

    public Color colorB, colorT, colorEmB, colorEmT;
    public float timeSwitch;
    private float a = 0;

    void Update() {
        if (a <= timeSwitch) {
            GetComponent<Renderer>().material.color = Color.Lerp(colorB, colorT, a / timeSwitch);
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.Lerp(colorEmB, colorEmT, a / timeSwitch));
            a += Time.deltaTime;
        }
    }
}
