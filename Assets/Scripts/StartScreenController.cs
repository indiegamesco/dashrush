﻿using UnityEngine;

public class StartScreenController : MonoBehaviour {

    public GameObject player, moveGui, audioManager;
    public static bool retry = false;

	public void View () {
        if (!retry) {
            gameObject.SetActive(true);
            moveGui.GetComponent<MoveGUI>().MoveUp();
        }
        else {
            player.GetComponent<PlayerController>().StartGame();
        }
	}

	public void Skip () {
        moveGui.GetComponent<MoveGUI>().MoveDown();
    }

	public void Click () {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
		Skip ();
        player.GetComponent<PlayerController> ().StartGame ();
	}
}
