﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public float speedLerp;
    private float distance;

    void Start()
    {   
        distance = player.transform.position.x - transform.position.x;
    }

    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x - distance,
                                                                           transform.position.y,
                                                                           transform.position.z), Time.deltaTime * speedLerp);
    }
}
