﻿using UnityEngine;
using UnityEngine.UI;

public class MoveGUI : MonoBehaviour {

    public float speed;
    public Canvas canvas;
    public int distance = 5000;
    protected bool moveUp = false, moveDown = false;

    void Update()
    {
        if (moveUp && GetComponent<RectTransform>().localPosition.z > 0)
        {
            GetComponent<RectTransform>().localPosition = new Vector3(GetComponent<RectTransform>().localPosition.x,
                                                                      GetComponent<RectTransform>().localPosition.y,
                                                                      GetComponent<RectTransform>().localPosition.z - speed);
            for (int i = 0; i < 5; i++) {
                canvas.GetComponent<CanvasGroup>().alpha = canvas.GetComponent<CanvasGroup>().alpha + 1 / (5000 / speed);
            }
        }
        if (moveDown && GetComponent<RectTransform>().localPosition.z > -5000)
        {
            GetComponent<RectTransform>().localPosition = new Vector3(GetComponent<RectTransform>().localPosition.x,
                                                                      GetComponent<RectTransform>().localPosition.y,
                                                                      GetComponent<RectTransform>().localPosition.z - 50);
            for (int i = 0; i < 5; i++)
            {
                canvas.GetComponent<CanvasGroup>().alpha = canvas.GetComponent<CanvasGroup>().alpha - 1 / 50;
            }
        }
    }

    public void MoveUp()
    {
       GetComponent<RectTransform>().localPosition = new Vector3(GetComponent<RectTransform>().localPosition.x,
                                                                  GetComponent<RectTransform>().localPosition.y,
                                                                  distance);
        moveUp = true;
        moveDown = false;
    }

    public void MoveDown()
    {
        moveUp = false;
        moveDown = true;
    }
}
