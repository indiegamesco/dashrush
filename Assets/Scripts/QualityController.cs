﻿using UnityEngine;
using System.Collections;

public class QualityController : MonoBehaviour {

    private static float time, nFrame;
    private static bool switchQuality = false;

    void Update() {
        if (!switchQuality) {
            time += Time.deltaTime;
            nFrame++;
            if (time >= 5)
            {
                SetQuality();
                time = 0;
                nFrame = 0;
                switchQuality = true;
            }
        }
    }

    void SetQuality() {
        if (nFrame / time < 25) {
            QualitySettings.SetQualityLevel(6, true);
        } else {
            QualitySettings.SetQualityLevel(7, true);
        }
    }
}
