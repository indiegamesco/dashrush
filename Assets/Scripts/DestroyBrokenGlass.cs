﻿using UnityEngine;
using System.Collections;

public class DestroyBrokenGlass : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "BrokenGlass(Clone)") {
            Destroy(other.gameObject);
        }
    }
}
